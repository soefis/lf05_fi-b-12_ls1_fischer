
public class AB01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.print("Das ist Beispielsatz 1\n");
		System.out.print("\n \n \"Das ist Beispielsatz 2\"\n\n\n");
		
		int alter = 1;
		String name = " Beispielsatz ";
		System.out.print("Das ist" + name + " " + alter +" ");
		
		//der Befehl "System.out.println("")" gibt den in der Klammer geschriebenen Text aus. Bei mehreren dieser Befehle werden die Texte untereinander ausgegeben.
		//der Befehl "System.out.print("")" gibt ebenfalls Text aus. Bei mehreren dieser Befehle ohne Zusatz werden die Texte hintereinander in einer Reihe geschrieben.

	}

}
