
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Ganze Zahlen
		// d steht f�r Dezimalzahl
		
		System.out.printf("| %10d%10d |",123456, -123456);
		
		// Kommazahlen
		// f steht f�r Kommazahlen
		
		System.out.printf("|%10.2f|",12.123456789);
		
		//Zeichenketten
		
		System.out.printf("|%-10.3s|","Max Mustermann");
		
		System.out.printf("Name:%-10sAlter:%-8dGewicht:%10.2f","Max",18,80.50);

	}

}
