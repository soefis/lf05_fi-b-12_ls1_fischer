import java.util.Scanner;

public class Fahrkartenautomat_korrigiert {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		double r�ckgabebetrag;
		double einzelpreisTickets;
		double anzahlTickets;

		einzelpreisTickets = 5.0;
		anzahlTickets = tastatur.nextDouble();
		eingezahlterGesamtbetrag = 0.0;
		zuZahlenderBetrag = anzahlTickets *= einzelpreisTickets;
		r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		eingeworfeneM�nze = tastatur.nextDouble();

		fahrscheinbestellung(anzahlTickets, einzelpreisTickets, zuZahlenderBetrag);

		fahrscheinbezahlung(eingezahlterGesamtbetrag, zuZahlenderBetrag, eingeworfeneM�nze);

		fahrscheinausgabe();

		rueckgeldAusgeben(r�ckgabebetrag);

	}

	public static void rueckgeldAusgeben(double r�ckgabebetrag) {
		if (r�ckgabebetrag > 0.0) {
			System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
		}
	}

	public static void fahrscheinbestellung(double anzahlTickets, double einzelpreisTickets, double zuZahlenderBetrag) {

		Scanner tastatur = new Scanner(System.in);

		System.out.print("Der Preis f�r ein Ticket betr�gt " + (einzelpreisTickets));

		System.out.print("\n\nAnzahl der  Tickets ausw�hlen: ");
		anzahlTickets = tastatur.nextDouble();

		System.out.print("Zu zahlender Betrag in Euro: " + (einzelpreisTickets * anzahlTickets));
		zuZahlenderBetrag = anzahlTickets *= einzelpreisTickets;
	}

	public static void fahrscheinbezahlung(double eingezahlterGesamtbetrag, double zuZahlenderBetrag,
			double eingeworfeneM�nze) {

		Scanner tastatur = new Scanner(System.in);

		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.println("\n\nNoch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 10 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

	}

	public static void fahrscheinausgabe() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}
}
