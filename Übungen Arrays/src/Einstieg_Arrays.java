import java.util.Scanner;

public class Einstieg_Arrays {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		int[] nmr = new int [5];
		
		nmr[0] = tastatur.nextInt();
		nmr[1] = tastatur.nextInt();
		nmr[2] = tastatur.nextInt();
		nmr[3] = tastatur.nextInt();
		nmr[4] = tastatur.nextInt();
		
		for(int n = nmr.length-1; n >= 0; n--) {
			
			System.out.println(nmr[n]);
			
		}

	}

}
