import java.util.Scanner;

public class Wiederholung {

	public static void main(String[] args) {
		
		int zahl01;
		int zahl02;
		int zahl03;
		
		Scanner Tastatur = new Scanner (System.in);
		
		System.out.println ("Dieses Programm rechnet Zahlen zusammen. \n");
		
		System.out.println ("Bitte geben Sie die erste Zahl ein: ");
		zahl01 = Tastatur.nextInt();
		
		System.out.println ("\nBitte geben Sie die zweite Zahl ein: ");
		zahl02 = Tastatur.nextInt();
		
		System.out.println ("\nBitte geben Sie die dritte Zahl ein: ");
		zahl03 = Tastatur.nextInt();
		
		int summe = zahl01 + zahl02 + zahl03;
		
		System.out.println ("\nDas Ergebnis der eingegebenen Zahlen lautet: " + summe);

	}

}
